import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import firebase from 'firebase';
class ForgotPassword extends Component {
  state = {
    email: '',
    error: null
  };
  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const { email } = this.state;
    firebase
      .auth()
      .sendPasswordResetEmail(email).then(function () {
        // Email sent.
      }).catch((error) => {
        this.setState({ error: error });
      });
  };
  render() {
    const { email, error } = this.state;
    return (
      <div>
        <div>
          {error ? (
            <h1>{error.message}</h1>
          ) : null}
        </div>
        <div>
          <form onSubmit={this.handleSubmit}>
            <input type="text" name="email" placeholder="Email" value={email} onChange={this.handleInputChange} />
            <button children="Confirm" />
          </form>
        </div>
      </div>
    );
  }
}
export default withRouter(ForgotPassword);