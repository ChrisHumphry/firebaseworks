import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBaFHi_1CMMjRIhyOw1ZgoqUGJ4QZmSeIA",
    authDomain: "project-b446f.firebaseapp.com",
    databaseURL: "https://project-b446f.firebaseio.com",
    projectId: "project-b446f",
    storageBucket: "",
    messagingSenderId: "272430681743",
    appId: "1:272430681743:web:a138e9f51ca52ad6"
};

firebase.initializeApp(firebaseConfig);

export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const messaging = firebase.messaging();